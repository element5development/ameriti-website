<?php 
/*----------------------------------------------------------------*\

	Template Name: Contact
	
\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/elements/navigation'); ?>

<?php get_template_part('template-parts/sections/contact-header'); ?>

<main id="main-content">
	<?php if( have_rows('article') ):  ?>
		<article>
			<?php 
			/*----------------------------------------------------------------*\
			|
			| Insert page content which is most often handled via ACF Pro
			| and highly recommend the use of the flexiable content so
			|	we already placed that code here.
			|
			| https://www.advancedcustomfields.com/resources/flexible-content/
			|
			\*----------------------------------------------------------------*/
			?>
			<?php
				while ( have_rows('article') ) : the_row();
					if( get_row_layout() == 'editor' ):
						get_template_part('template-parts/sections/article/editor');
					elseif( get_row_layout() == '2editor' ):
						get_template_part('template-parts/sections/article/editor-2-column');
					elseif( get_row_layout() == '3editor' ):
						get_template_part('template-parts/sections/article/editor-3-column');
					elseif( get_row_layout() == 'media+text' ):
						get_template_part('template-parts/sections/article/media-text');
					elseif( get_row_layout() == 'cover' ):
						get_template_part('template-parts/sections/article/cover');
					elseif( get_row_layout() == 'gallery' ):
						get_template_part('template-parts/sections/article/gallery');
					elseif( get_row_layout() == 'card_grid' ):
						get_template_part('template-parts/sections/article/card-grid');
					elseif( get_row_layout() == 'card_grid_gray' ):
						get_template_part('template-parts/sections/article/card-grid-gray');
					elseif( get_row_layout() == 'division_repeater' ):
						get_template_part('template-parts/sections/article/division-repeater');
					elseif( get_row_layout() == 'anchor' ):
						get_template_part('template-parts/sections/article/anchor');
					elseif( get_row_layout() == 'news' ):
						get_template_part('template-parts/sections/article/news');
					endif;
				endwhile;
			?>
		</article>
	<?php else : ?>
		<article>
			<section class="is-narrow">
				<h2>This page has no content.</h2>
			</section>
		</article>
	<?php endif; ?>
</main>

<?php get_template_part('template-parts/sections/post-footer'); ?>

<?php get_footer(); ?>