<?php
/*----------------------------------------------------------------*\
	INITIALIZE MENUS
\*----------------------------------------------------------------*/
function nav_creation() {
	$locations = array(
		'primary_navigation' => __( 'Primary Menu' ),
		'footer_navigation' => __( 'Footer Menu' ),
		'footer_navigation_2' => __( 'Footer Menu 2' ),
		'footer_navigation_3' => __( 'Footer Menu 3' ),
	);
	register_nav_menus( $locations );
}
add_action( 'init', 'nav_creation' );

/*----------------------------------------------------------------*\
	ENABLE SUBMENU DESCRIPTIONS
\*----------------------------------------------------------------*/
/**
 * @param string  $item_output The menu item output.
 * @param WP_Post $item        Menu item object.
 * @param int     $depth       Depth of the menu.
 * @param array   $args        wp_nav_menu() arguments.
 * @return string Menu item with possible description.
 */
function add_nav_description( $item_output, $item, $depth, $args ) {
	if ( 'primary_navigation' == $args->theme_location && $item->description ) {
		$item_output = str_replace( $args->link_after . '</a>', '<div class="menu-item-description">' . esc_html( $item->description ) . '</div><p>Learn More</p>' . $args->link_after . '</a>', $item_output );
	}
	
	return $item_output;
}
add_filter( 'walker_nav_menu_start_el', 'add_nav_description', 10, 4 );