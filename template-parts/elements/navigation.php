<?php 
/*----------------------------------------------------------------*\

	PRIMARY NAVIGATION

\*----------------------------------------------------------------*/
?>
<div class="primary-navigation">
	<nav>
		<?php $logo = get_field('logo', 'options'); ?>
		<a href="<?php echo get_home_url(); ?>">
			<img src="<?php echo $logo['sizes']['medium']; ?>" alt="<?php echo $logo['alt']; ?>">
		</a>
		<?php wp_nav_menu(array( 'theme_location' => 'primary_navigation' )); ?>
		<button class="activate-search">
			<svg>
				<use xlink:href="#search" />
			</svg>
		</button>
		<button class="activate-menu">
			<svg>
				<use xlink:href="#menu" />
			</svg>
			Menu
		</button>
		<button class="close-menu">Close</button>
		<div class="search-form">
			<?php echo get_search_form(); ?>
		</div>
	</nav>
</div>