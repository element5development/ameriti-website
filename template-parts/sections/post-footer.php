<?php 
/*----------------------------------------------------------------*\

	POST FOOTER
	Display copyright and navigation

\*----------------------------------------------------------------*/
?>

<footer class="post-footer">
	<div>
		<div>
			<?php $footerlogo = get_field('footer_logo', 'options'); ?>
			<a href="<?php echo get_home_url(); ?>">
				<img src="<?php echo $footerlogo['sizes']['medium']; ?>" alt="<?php echo $footerlogo['alt']; ?>">
			</a>
			<a class="button" href="tel:+1<?php $remove = array(" ","(",")",".","-"); echo str_replace($remove, "", get_field('phone', 'options')); ?>"><?php the_field('phone', 'options'); ?></a>
		</div>
		<?php wp_nav_menu(array( 'theme_location' => 'footer_navigation' )); ?>
		<?php wp_nav_menu(array( 'theme_location' => 'footer_navigation_2' )); ?>
		<?php wp_nav_menu(array( 'theme_location' => 'footer_navigation_3' )); ?>
		<div class="social-links">
		<?php if ( get_field('facebook', 'options') ) : ?>
			<a href="<?php the_field('facebook', 'options'); ?>" target="_blank">
				<svg>
					<use xlink:href="#facebook" />
				</svg>
			</a>
		<?php endif; ?>
		<?php if ( get_field('twitter', 'options') ) : ?>
			<a href="<?php the_field('twitter', 'options'); ?>" target="_blank">
				<svg>
					<use xlink:href="#twitter" />
				</svg>
			</a>
		<?php endif; ?>
		<?php if ( get_field('linkedin', 'options') ) : ?>
			<a href="<?php the_field('linkedin', 'options'); ?>" target="_blank">
				<svg>
					<use xlink:href="#linkedin" />
				</svg>
			</a>
		<?php endif; ?>
		</div>
	</div>
	<div class="copyright">
		<a href="<?php echo get_home_url(); ?>/terms-and-conditions/">Terms and Conditions</a>
		<p>©<?php echo date('Y'); ?> <?php echo get_bloginfo( 'name' ); ?>. All Rights Reserved. <a href="<?php echo get_home_url(); ?>/privacy-policy/">Privacy Policy.</a></p>
	</div>
</footer>