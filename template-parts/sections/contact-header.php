<?php 
/*----------------------------------------------------------------*\

	CONTACT HEADER
	Display the post title

\*----------------------------------------------------------------*/
?>

<header class="post-head">
	<div>
		<?php if ( get_field('title') ) : ?>
			<h1><?php the_field('title'); ?></h1>
		<?php else : ?>
			<h1><?php the_title(); ?></h1>
		<?php endif; ?>

		<?php $formid = get_field('gravity_form_id'); ?>
		<?php echo do_shortcode( '[gravityform id="'. $formid .'" title="true" description="false"]' ); ?>
	</div>
</header>