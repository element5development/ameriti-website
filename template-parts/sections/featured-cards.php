<?php 
/*----------------------------------------------------------------*\

	FEATURED CARDS
	displaying three featured cards

\*----------------------------------------------------------------*/
?>
<section class="featured-cards">
	<div class="is-extra-wide">
		<article>
			<h3><?php the_field('title_1'); ?></h3>
			<p><?php the_field('description_1'); ?></p>
			<?php 
				$link = get_field('link_1');
			?>
			<a class="button is-ghost" href="<?php echo $link['url']; ?>"><?php echo $link['title']; ?></a>
			<a href="<?php echo $link['url']; ?>"></a>
		</article>
		<article>
			<h3><?php the_field('title_2'); ?></h3>
			<p><?php the_field('description_2'); ?></p>
			<?php 
				$link = get_field('link_2');
			?>
			<a class="button is-ghost" href="<?php echo $link['url']; ?>"><?php echo $link['title']; ?></a>
			<a href="<?php echo $link['url']; ?>"></a>
		</article>
		<article>
			<h3><?php the_field('title_3'); ?></h3>
			<p><?php the_field('description_3'); ?></p>
			<?php 
				$link = get_field('link_3');
			?>
			<a class="button is-ghost" href="<?php echo $link['url']; ?>"><?php echo $link['title']; ?></a>
			<a href="<?php echo $link['url']; ?>"></a>
		</article>
	</div>
	<div class="standard eagle-top">
		<p><?php the_field('bottom_paragraph'); ?></p>
	</div>
</section>