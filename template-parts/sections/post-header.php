<?php 
/*----------------------------------------------------------------*\

	POST HEADER
	Display the post title

\*----------------------------------------------------------------*/
?>

<header class="post-head">
	<div>
		<?php if ( get_field('title') ) : ?>
			<h1><?php the_field('title'); ?></h1>
		<?php else : ?>
			<h1><?php the_title(); ?></h1>
		<?php endif; ?>
		
		<?php
			$headerimage = get_field('header_image');
		?>
		<div class="image-container">
			<?php if( $headerimage ): ?>
				<img class="lazyload blur-up" data-expand="200" data-sizes="auto" src="<?php echo $headerimage['sizes']['placeholder']; ?>" data-src="<?php echo $headerimage['sizes']['large']; ?>" data-srcset="<?php echo $headerimage['sizes']['small']; ?> 350w, <?php echo $headerimage['sizes']['medium']; ?> 700w, <?php echo $headerimage['sizes']['large']; ?> 1000w, <?php echo $headerimage['sizes']['xlarge']; ?> 1200w"  alt="<?php echo $headerimage['alt']; ?>">
			<?php else : ?>
				<img class="lazyload blur-up" data-expand="200" data-sizes="auto" src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/default-image.jpg" data-src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/default-image.jpg" data-srcset="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/default-image.jpg 350w, <?php echo get_stylesheet_directory_uri(); ?>/dist/images/default-image.jpg 700w, <?php echo get_stylesheet_directory_uri(); ?>/dist/images/default-image.jpg 1000w, <?php echo get_stylesheet_directory_uri(); ?>/dist/images/default-image.jpg 1200w"  alt="AmeriTi Manufacturing">
			<?php endif; ?>
			<div class="overlay"></div>
		</div>
	</div>
</header>