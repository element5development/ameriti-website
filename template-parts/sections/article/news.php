<?php 
/*----------------------------------------------------------------*\

	ARTICLE SECTION
	displaying the most recent news post

\*----------------------------------------------------------------*/
?>

<?php if( get_sub_field('show_news') ) { ?>

<section class="anchor standard">
	<h3>AmeriTi News</h3>
</section>
<?php if (have_posts()) : ?>
	<section class="is-wide news">
		<?php
		$args = array(
			'post_type' => 'post',
			'posts_per_page' => 1,
		);
		$loop = new WP_Query( $args );
		?>
		<?php while ( $loop->have_posts() ) : $loop->the_post();?>
			<article class="archive-result <?php echo $post_type; ?>">
				<header>
					<?php	the_post_thumbnail( 'medium_large' ); ?>
				</header>
				<div class="entry-content">
					<h2><?php the_title(); ?></h2>
					<?php the_excerpt(); ?>
					<button>Continue Reading</button>
				</div>
				<a href="<?php the_permalink(); ?>"></a>
			</article>
		<?php endwhile; ?>
		<?php wp_reset_query(); ?>
	</section>
<?php endif; ?>

<?php } ?>