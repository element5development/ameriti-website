<?php 
/*----------------------------------------------------------------*\

	ARTICLE SECTION
	displaying divisions of AmeriTi

\*----------------------------------------------------------------*/
?>

<section class="division <?php the_sub_field('width'); ?>">
	<h3><?php the_sub_field('division'); ?></h3>

	<div class="contact-repeater">
		<?php while ( have_rows('contact_repeater') ) : the_row(); ?>
			<article>
				<h4><?php the_sub_field('contact'); ?></h4>
				<p><?php the_sub_field('position'); ?></p>
				<?php 
					$extension = get_sub_field('phone_extension');
				?>
				<?php if( $extension ): ?>
					<a href="tel:+13133665300p<?php echo $extension; ?>">Ext <?php echo $extension; ?></a>
				<?php endif; ?>
			</article>
		<?php endwhile; ?>
	</div>
</section>