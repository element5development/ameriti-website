<?php 
/*----------------------------------------------------------------*\

	ARTICLE SECTION
	displaying anchor link

\*----------------------------------------------------------------*/
?>
<?php 
	$anchor = get_sub_field('anchor');
?>
<section class="anchor <?php the_sub_field('width'); ?>">
	<h3 id="<?php echo str_replace(' ', '-', strtolower($anchor)); ?>"><?php the_sub_field('anchor'); ?></h3>
</section>