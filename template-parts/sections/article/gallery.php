<?php 
/*----------------------------------------------------------------*\

	ARTICLE SECTION
	displaying gallery of images

\*----------------------------------------------------------------*/
?>

<?php //NUMBER TO TEXT
	$number = new NumberFormatter("en", NumberFormatter::SPELLOUT);
	$column_count = $number->format(get_sub_field('columns'));
?>

<?php //GALLERY
  $images = get_sub_field('gallery');
  for ($i=0; $i<count($images); $i++) {
    // the id of the image is in $images[$i]['ID']
    $images[$i]['image_link'] = get_field('image_link', $images[$i]['id']);
  }
?>

<section class="gallery <?php the_sub_field('width'); ?> <?php echo $column_count; ?>-columns">
	<?php foreach( $images as $image ): ?>
		<figure>
			<a href="<?php echo $image['image_link']; ?>" target="_blank">
				<img class="lazyload blur-up" data-expand="200" data-sizes="auto" src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['large']; ?>" data-srcset="<?php echo $image['sizes']['small']; ?> 350w, <?php echo $image['sizes']['medium']; ?> 700w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['xlarge']; ?> 1200w"  alt="<?php echo $image['alt']; ?>">
			</a>
		</figure>
	<?php endforeach; ?>
</section>
