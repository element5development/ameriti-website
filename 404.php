<?php 
/*----------------------------------------------------------------*\

	ERROR / NO PAGE FOUND

\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/elements/navigation'); ?>

<main id="main-content">
	<article>
		<section class="standard">
			<h1>This page can't be found.</h1>
		</section>
	</article>
	<aside class="acf-related-posts standard">
		<section class="standard">
			<h3>Helpful Links</h3>
		</section>
		<article class="related-relationship">
			<a class="button" href="<?php echo get_site_url(); ?>">Home</a>
		</article>
		<article class="related-relationship">
			<a class="button" href="<?php echo get_site_url(); ?>/service/">Service</a>
		</article>
		<article class="related-relationship">
			<a class="button" href="<?php echo get_site_url(); ?>/applications/">Applications</a>
		</article>
	</aside>
</main>

<?php get_template_part('template-parts/sections/post-footer'); ?>

<?php get_footer(); ?>