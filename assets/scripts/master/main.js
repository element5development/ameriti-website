var $ = jQuery;

$(document).ready(function () {
	/*----------------------------------------------------------------*\
		SELECT FIELD PLACEHOLDER
	\*----------------------------------------------------------------*/
	$(function () {
		$('select').addClass('has-placeholder');
	});
	$("select").change(function () {
		$(this).removeClass('has-placeholder');
	});
	/*----------------------------------------------------------------*\
		FILE UPLOAD
	\*----------------------------------------------------------------*/
	$('label').each(function () {
		if ($(this).siblings('.ginput_container_fileupload').length) {
			$(this).addClass('file-upload-label');
		}
	});

	if ($('input[type="file"]').length > 0) {
		var fileInput = document.querySelector("input[type='file']");
		var button = document.querySelector("input[type='file']+span");
		fileInput.addEventListener("change", function (event) {
			button.innerHTML = this.value;
			$('label.file-upload-label').addClass("file-uploaded");
		});
	}
	/*----------------------------------------------------------------*\
		SHOW NAV ON SCROLL UP
	\*----------------------------------------------------------------*/
	$(window).scroll(function () {
		var currentTop = $(window).scrollTop();
		if (currentTop <= 0) {
			$(".primary-navigation").removeClass('nav-up');
			$(".primary-navigation").removeClass('nav-down');
		} else if (currentTop < this.previousTop) {
			$(".primary-navigation").removeClass('nav-up');
			$(".primary-navigation").addClass('nav-down');
		} else {
			$(".primary-navigation").removeClass('nav-down');
			$(".primary-navigation").addClass('nav-up');
		}
		this.previousTop = currentTop;
	});
	/*----------------------------------------------------------------*\
  	ACTIVATE SEARCH
	\*----------------------------------------------------------------*/
	$("button.activate-search").click(function () {
		$(".search-form").addClass("is-active");
		$(".search-form form input").focus();
		$("#menu-primary-navigation").addClass("search-active");
		$(".activate-search").addClass("search-active");
		$(".activate-menu").addClass("search-active");
		$(".primary-navigation nav > a").addClass("search-active");
	});
	$(document).click(function (event) {
		//if you click on anything except the search input, search button, or search open button, close the search
		if (!$(event.target).closest(".search-form form input, .search-form form button, button.activate-search").length) {
			$(".search-form").removeClass("is-active");
			$("#menu-primary-navigation").removeClass("search-active");
			$(".activate-search").removeClass("search-active");
			$(".activate-menu").removeClass("search-active");
			$(".primary-navigation nav > a").removeClass("search-active");
		}
	});

	/*----------------------------------------------------------------*\
  	ACTIVATE MENU
	\*----------------------------------------------------------------*/
	$("button.activate-menu").click(function () {
		$("#menu-primary-navigation").addClass("is-active");
		$("button.close-menu").addClass("is-active");
	});
	$("button.close-menu").click(function () {
		$("#menu-primary-navigation").removeClass("is-active");
		$(this).removeClass("is-active");
	});
	$(document).click(function (event) {
		//if you click on anything except within the open menu, close the menu
		if (!$(event.target).closest("#menu-primary-navigation, button.activate-menu").length) {
			$("#menu-primary-navigation").removeClass("is-active");
			$("button.close-menu").removeClass("is-active");
		}
	});
	/*----------------------------------------------------------------*\
		SUBMENU SHOW/HIDE
	\*----------------------------------------------------------------*/
	$(".menu-item-496").click(function () {
		$(this).toggleClass("is-active");
		$(".menu-item-495").removeClass("is-active");
		$("body").addClass("submenu-active");
	});
	$(".menu-item-495").click(function () {
		$(this).toggleClass("is-active");
		$(".menu-item-496").removeClass("is-active");
		$("body").addClass("submenu-active");
	});
	$('.menu-item-has-children > a').click(function() {
    event.preventDefault();
	});
	$(document).click(function (event) {
		//if you click on anything except within the open menu, close the menu
		if (!$(event.target).closest(".menu-item-has-children").length) {
			$(".menu-item-has-children").removeClass("is-active");
			$("body").removeClass("submenu-active");
		}
	});
	/*----------------------------------------------------------------*\
  	LOGIC FOR LOAD MORE BUTTON
	\*----------------------------------------------------------------*/
	if ($('.next.page-numbers').length) {
		$('.load-more').css('display', 'table');
	}
	/*----------------------------------------------------------------*\
		INFINITE SCROLL INIT
	\*----------------------------------------------------------------*/
	$('.feed').infiniteScroll({
		path: '.next.page-numbers',
		append: '.archive-result',
		button: '.load-more',
		scrollThreshold: false,
		checkLastPage: true,
		status: '.page-load-status',
		history: false,
	});
	/*----------------------------------------------------------------*\
		CREATE ANCHOR MENU
	\*----------------------------------------------------------------*/
	$("main > article h3").each(function() {
		var anchor = $(this).attr('id');
		var anchortext = anchor.replace(/-/g, " ");
		$('#main-content > article nav.anchors').append('<a href="#' + anchor + '">' + anchortext + '</a>');
	});

});