<?php 
/*----------------------------------------------------------------*\

	DEFAULT POST ARCHIVE TEMPLATE

\*----------------------------------------------------------------*/
?>

<?php 
	$post_type = get_query_var('post_type'); 
	if ( $post_type == '' ) {
		$post_type = 'post';
	}
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/elements/navigation'); ?>

<header class="post-head">
	<div>
		<?php if ( get_field($post_type.'_title','options') ) : ?>
			<h1><?php the_field($post_type.'_title','options'); ?></h1>
		<?php else : ?>
			<h1>AmeriTi News</h1>
		<?php endif; ?>
		
		<?php
			$headerimage = get_field($post_type.'_header_image','options');
		?>
		<?php if( $headerimage ): ?>
			<img class="lazyload blur-up" data-expand="200" data-sizes="auto" src="<?php echo $headerimage['sizes']['placeholder']; ?>" data-src="<?php echo $headerimage['sizes']['large']; ?>" data-srcset="<?php echo $headerimage['sizes']['small']; ?> 350w, <?php echo $headerimage['sizes']['medium']; ?> 700w, <?php echo $headerimage['sizes']['large']; ?> 1000w, <?php echo $headerimage['sizes']['xlarge']; ?> 1200w"  alt="<?php echo $headerimage['alt']; ?>">
		<?php endif; ?>
	</div>
</header>

<main id="main-content">
	<article>
		<?php if (get_field($post_type.'_intro','options')) : ?>
			<section class="standard intro">
				<?php the_field($post_type.'_intro','options'); ?>
			</section>
		<?php endif; ?>
		<?php if (have_posts()) : ?>
			<section class="is-wide feed">
				<?php	while ( have_posts() ) : the_post(); ?>
					<article class="archive-result <?php echo $post_type; ?>">
						<header>
							<?php	the_post_thumbnail( 'medium_large' ); ?>
						</header>
						<div class="entry-content">
							<h2><?php the_title(); ?></h2>
							<?php the_excerpt(); ?>
							<button>Continue Reading</button>
						</div>
						<a href="<?php the_permalink(); ?>"></a>
					</article>
				<?php endwhile; ?>
			</section>
			<section class="infinite-scroll standard">
				<div class="page-load-status">
					<p class="infinite-scroll-request">
						<svg class="loading" x="0px" y="0px" width="40px" height="40px" viewBox="0 0 50 50" style="enable-background:new 0 0 50 50;">
							<path d="M43.935,25.145c0-10.318-8.364-18.683-18.683-18.683c-10.318,0-18.683,8.365-18.683,18.683h4.068c0-8.071,6.543-14.615,14.615-14.615c8.072,0,14.615,6.543,14.615,14.615H43.935z">
								<animateTransform attributeType="xml"
									attributeName="transform"
									type="rotate"
									from="0 25 25"
									to="360 25 25"
									dur="0.6s"
									repeatCount="indefinite"/>
							</path>
						</svg>
					</p>
					<p class="infinite-scroll-last"></p>
					<p class="infinite-scroll-error"></p>
				</div>
				<?php the_posts_pagination( array(
					'prev_text'	=> __( 'Previous page' ),
					'next_text'	=> __( 'Next page' ),
				) ); ?>
				<a class="load-more button is-red">Load More</a>
			</section>
		<?php else : ?>
			<article>
				<section class="is-narrow">
					<p>This page has no content.</p>
				</section>
			</article>
		<?php endif; ?>
	</article>
</main>

<?php get_template_part('template-parts/sections/post-footer'); ?>

<?php get_footer(); ?>