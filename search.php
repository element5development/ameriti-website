<?php 
/*----------------------------------------------------------------*\

	SEARCH RESULTS ARCHIVE

\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/elements/navigation'); ?>

<header class="post-head">
	<div>
		<h1>Search results for:</h1>
		<h2><?php the_search_query(); ?></h2>
	</div>
</header>

<main id="main-content">
	<?php if (have_posts()) : ?>
		<?php	while ( have_posts() ) : the_post(); ?>
			<article class="search-result standard <?php echo get_post_type(); ?>">
				<header>
					<h3><?php the_title(); ?></h3>
				</header>
				<section class="entry-content">
					<?php the_excerpt(); ?>
					<button>Continue Reading</button>
				</section>
				<a href="<?php the_permalink(); ?>"></a>
			</article>
		<?php endwhile; ?>
	<?php else : ?>
		<article>
			<section class="standard">
				<p>We cannot find anything for "<?php echo(get_search_query()); ?>".</p>
				<?php get_search_form( $echo ); ?>
			</section>
		</article>
	<?php endif; ?>
</main>

<?php get_template_part('template-parts/sections/post-footer'); ?>

<?php get_footer(); ?>