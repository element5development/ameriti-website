<?php 
/*----------------------------------------------------------------*\

	DEFAULT SINGLE POST TEMPLATE
	This is the page template for the post, for the preview
	look under template-parts.

\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/elements/navigation'); ?>

<header class="post-head">
	<div>
		<?php if ( get_field('title') ) : ?>
			<h1><?php the_field('title'); ?></h1>
		<?php else : ?>
			<h1><?php the_title(); ?></h1>
		<?php endif; ?>
		
		<?php if( has_post_thumbnail() ): ?>
			<?php the_post_thumbnail( 'large' ); ?>
		<?php endif; ?>
	</div>
</header>

<main id="main-content">
	<?php if( have_rows('article') ):  ?>
	<article>
		<nav id="anchors" class="anchors"></nav>
		<?php 
			/*----------------------------------------------------------------*\
			|
			| Insert page content which is most often handled via ACF Pro
			| and highly recommend the use of the flexiable content so
			|	we already placed that code here.
			|
			| https://www.advancedcustomfields.com/resources/flexible-content/
			|
			\*----------------------------------------------------------------*/
			?>
		<?php
			while ( have_rows('article') ) : the_row();
				if( get_row_layout() == 'editor' ):
					get_template_part('template-parts/sections/article/editor');
				elseif( get_row_layout() == '2editor' ):
					get_template_part('template-parts/sections/article/editor-2-column');
				elseif( get_row_layout() == '3editor' ):
					get_template_part('template-parts/sections/article/editor-3-column');
				elseif( get_row_layout() == 'media+text' ):
					get_template_part('template-parts/sections/article/media-text');
				elseif( get_row_layout() == 'cover' ):
					get_template_part('template-parts/sections/article/cover');
				elseif( get_row_layout() == 'gallery' ):
					get_template_part('template-parts/sections/article/gallery');
				elseif( get_row_layout() == 'card_grid' ):
					get_template_part('template-parts/sections/article/card-grid');
				elseif( get_row_layout() == 'card_grid_gray' ):
					get_template_part('template-parts/sections/article/card-grid-gray');
				elseif( get_row_layout() == 'division_repeater' ):
					get_template_part('template-parts/sections/article/division-repeater');
				elseif( get_row_layout() == 'anchor' ):
					get_template_part('template-parts/sections/article/anchor');
				elseif( get_row_layout() == 'news' ):
					get_template_part('template-parts/sections/article/news');
				endif;
			endwhile;
		?>
	</article>
	<a class="up-button" href="#anchors">
		<svg>
			<use xlink:href="#up" />
		</svg>
	</a>
	<?php else : ?>
	<article>
		<section class="is-narrow">
			<h2>This page has no content.</h2>
		</section>
	</article>
	<?php endif; ?>
</main>

<?php get_template_part('template-parts/sections/post-footer'); ?>

<?php get_footer(); ?>